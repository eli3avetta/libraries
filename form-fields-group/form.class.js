/*
FormClass {constructor(formELem, inputsWithParams)}
  formELem - главная форма с тегом <form></form> из HTML
  inputsWithParams - массив с объектами, содержащими в себе name, validation и value инпута(ов);

  Пример:
  this.formClass = new FormClass(this.hostElemAsForm, [
      {
        name: 'lastName',
        validations: {
          required: {
            param: true,
          },
          minLength: {
            param: 3,
            message: 'Не менее 3-х символов'
          },
          maxLength: {
            param: 15,
            message: 'Не более 15-ти символов'
          },
        },
        value: 'Ваня'
      },
     }
  ]);

htmlGroup = this.formELem.elements[item.name] - HTML элемент/массив, содержащие в себе инпут/группу инпутов по name
makeValidationAllGroups() вызывается в момент отправки формы, и отдает валидны ли все элементы формы
*/

class FormClass {
  formELem;
  groups = {};

  constructor(formELem, inputsWithParams) {
    this.formELem = formELem;

    inputsWithParams.forEach(item => {
      const htmlGroup = this.formELem.elements[item.name];
      if (htmlGroup) {
        const fieldsGroup = new FieldsGroupClass(this.formELem, item.name, htmlGroup, item.validations, item.value);
        if (!this.groups[item.name]) {
          this.groups[item.name] = fieldsGroup;
        } else {
          console.error(`Дубликат группы с name: ${ item.name }`);
        }
      } else {
        console.error(`Группа с name: ${ item.name } не найдена.`);
      }
    })
  }

  makeValidationAllGroups() {
    let isValid = true;
    Object.keys(this.groups).forEach(key => {
      const group = this.groups[key];
      if (!group.makeValidation()) {
        isValid = false;
      }
    });
    return isValid;
  }

  markAsShowAllGroups() {
    Object.keys(this.groups).forEach(key => {
      const group = this.groups[key];
      group.markAsShow()
    });
  }

  resetShowAllGroups() {
    Object.keys(this.groups).forEach(key => {
      const group = this.groups[key];
      group.resetShow()
    });
  }
}
